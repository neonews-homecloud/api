package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.http.HttpHeaders.ContentDisposition
import io.ktor.http.HttpHeaders.LastModified
import io.ktor.http.HttpHeaders.Location
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.encodeURLPath
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.http.content.LocalFileContent
import io.ktor.server.http.httpDateFormat
import io.ktor.server.request.header
import io.ktor.server.request.receiveStream
import io.ktor.server.response.header
import io.ktor.server.response.respond
import io.ktor.server.response.respondText
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database
import java.io.FileNotFoundException
import java.time.Clock
import java.time.Instant
import java.time.Instant.now
import java.time.ZonedDateTime

suspend fun PipelineContext<Unit, ApplicationCall>.downloadFile(resource: WebDAVResource, filesystem: UserFilesystem) {
  if (filesystem.isDirectory(resource)) {
    return call.respondText("This is a WebDAV directory. Please use a WebDAV client to access it.")
  }

  val file = filesystem.read(resource) ?: return call.respond(NotFound)
  call.response.header(ContentDisposition, "attachment; filename=${file.name.encodeURLPath()}")
  call.respond(LocalFileContent(file, filesystem.contentType(resource)))
}

suspend fun PipelineContext<Unit, ApplicationCall>.uploadFile(
  resource: WebDAVResource,
  filesystem: UserFilesystem,
  database: Database,
  mutex: Mutex,
  clock: Clock
) {
  try {
    val lastModified =
      call.request.header(LastModified)?.let { ZonedDateTime.parse(it, httpDateFormat) }?.let { Instant.from(it) }
    filesystem.store(resource, call.receiveStream(), lastModified ?: now(clock), database, mutex)
    call.response.header(Location, filesystem.location(resource))
    call.respond(Created)
  } catch (e: FileNotFoundException) {
    call.respond(NotFound)
  }
}

suspend fun PipelineContext<Unit, ApplicationCall>.delete(resource: WebDAVResource, filesystem: UserFilesystem) {
  if (!filesystem.exists(resource)) {
    return call.respond(NotFound)
  }

  filesystem.delete(resource)
  call.respond(NoContent)
}
