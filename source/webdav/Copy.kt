package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.http.HttpHeaders.Depth
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.request.header
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database

suspend fun PipelineContext<Unit, ApplicationCall>.copy(
  resource: WebDAVResource,
  filesystem: UserFilesystem,
  database: Database,
  mutex: Mutex
) {
  val copy = when (call.request.header(Depth)) {
    "0" -> filesystem::copy
    "infinity" -> filesystem::copyRecursively
    else -> filesystem::copyRecursively
  }

  copyOrMove(resource, filesystem) { source, destination ->
    copy(source, destination, database, mutex)
  }
}
