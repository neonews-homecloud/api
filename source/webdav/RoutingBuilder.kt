package de.neonew.homecloud.webdav

import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Copy
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Mkcol
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Move
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Propfind
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Proppatch
import io.ktor.server.application.ApplicationCall
import io.ktor.server.routing.Route
import io.ktor.server.routing.method
import io.ktor.util.pipeline.PipelineInterceptor

fun Route.propfind(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Propfind) { handle(body) }

fun Route.proppatch(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Proppatch) { handle(body) }

fun Route.mkcol(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Mkcol) { handle(body) }

fun Route.copy(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Copy) { handle(body) }

fun Route.move(body: PipelineInterceptor<Unit, ApplicationCall>): Route =
  method(Move) { handle(body) }

