package de.neonew.homecloud.webdav

import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.proppatch() =
  call.respond(Forbidden)
