package de.neonew.homecloud.webdav

import de.neonew.homecloud.UserFilesystem
import io.ktor.http.HttpHeaders.Destination
import io.ktor.http.HttpHeaders.Location
import io.ktor.http.HttpHeaders.Overwrite
import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Conflict
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.PreconditionFailed
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.request.header
import io.ktor.server.response.header
import io.ktor.server.response.respond
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.copyOrMove(
  resource: WebDAVResource,
  filesystem: UserFilesystem,
  method: suspend (WebDAVResource, WebDAVResource) -> Unit
) {
  val destination = call.request.header(Destination)
    ?.toResource()
    ?: return call.respond(BadRequest)

  if (resource == destination) {
    return call.respond(Forbidden)
  }

  if (!filesystem.parentExists(destination)) {
    return call.respond(Conflict)
  }

  if (!filesystem.exists(resource)) {
    return call.respond(NotFound)
  }

  return copyOrMoveExistingResource(resource, destination, filesystem, method)
}

private suspend fun PipelineContext<Unit, ApplicationCall>.copyOrMoveExistingResource(
  resource: WebDAVResource,
  destination: WebDAVResource,
  filesystem: UserFilesystem,
  method: suspend (WebDAVResource, WebDAVResource) -> Unit
) =
  when (filesystem.exists(destination)) {
    true -> when (call.request.header(Overwrite)) {
      "F" -> call.respond(PreconditionFailed)
      else -> copyOrMove(resource, destination, NoContent, filesystem, method)
    }
    false -> copyOrMove(resource, destination, Created, filesystem, method)
  }

private suspend fun PipelineContext<Unit, ApplicationCall>.copyOrMove(
  resource: WebDAVResource,
  destination: WebDAVResource,
  status: HttpStatusCode,
  filesystem: UserFilesystem,
  method: suspend (WebDAVResource, WebDAVResource) -> Unit
) {
  try {
    method(resource, destination)
    call.response.header(Location, filesystem.location(destination))
    call.respond(status)
  } catch (e: FileSystemException) {
    call.respond(Forbidden)
  } catch (e: java.nio.file.FileSystemException) {
    call.respond(Forbidden)
  }
}
