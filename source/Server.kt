package de.neonew.homecloud

import com.natpryce.konfig.Configuration
import com.natpryce.konfig.ConfigurationProperties.Companion.systemProperties
import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.Key
import com.natpryce.konfig.intType
import com.natpryce.konfig.overriding
import com.natpryce.konfig.stringType
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import de.neonew.homecloud.api.ValidationException
import de.neonew.homecloud.authentication.basic
import de.neonew.homecloud.authentication.jwt
import de.neonew.homecloud.logging.TimedCallLogging
import de.neonew.homecloud.thumbnails.launchThumbnailGenerationConsumer
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.ApplicationStopped
import io.ktor.server.application.install
import io.ktor.server.auth.Authentication
import io.ktor.server.auth.basic
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.addShutdownHook
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import io.ktor.server.plugins.autohead.AutoHeadResponse
import io.ktor.server.plugins.conditionalheaders.ConditionalHeaders
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.response.respond
import io.ktor.server.routing.IgnoreTrailingSlash
import io.ktor.server.routing.routing
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Database.Companion.connect
import java.io.File
import java.time.Clock
import javax.sql.DataSource

fun main() {
  val server = server(config(), Clock.systemUTC())
  server.addShutdownHook {
    server.stop(1000, 5000)
  }
  server.start(wait = true)
}

fun server(config: Configuration, clock: Clock): ApplicationEngine {
  val port = config[Key("server.port", intType)]
  val filesystem = Filesystem(File(config[Key("directory", stringType)]))
  val webFrontend = File(config[Key("webFrontend", stringType)])
  val jwtSecret = config[Key("jwt.secret", stringType)]
  val database = connect(config)

  // This is super ugly. :(
  // https://github.com/JetBrains/Exposed/issues/587
  val sqliteWriteMutex = Mutex()

  val thumbnailGenerators = launchThumbnailGenerationConsumer(database, sqliteWriteMutex, filesystem)

  return embeddedServer(Jetty, port) {
    install(TimedCallLogging)
    install(ContentNegotiation) { json() }
    install(AutoHeadResponse)
    install(ConditionalHeaders)
    install(Authentication) {
      basic { }
      basic(database)
      jwt(database, clock, jwtSecret)
    }
    install(StatusPages) { exception<ValidationException> { call, _ -> call.respond(BadRequest) } }
    install(IgnoreTrailingSlash)

    routing { routes(database, sqliteWriteMutex, filesystem, webFrontend, clock, jwtSecret) }

    environment.monitor.subscribe(ApplicationStopped) {
      runBlocking { thumbnailGenerators.forEach { it.cancelAndJoin() } }
    }
  }
}

fun dataSource(config: Configuration): DataSource {
  val hikariConfig = HikariConfig()
  hikariConfig.driverClassName = config[Key("database.driver", stringType)]
  hikariConfig.jdbcUrl = config[Key("database.url", stringType)]
  hikariConfig.username = config.getOrElse(Key("database.username", stringType)) { "" }
  hikariConfig.password = config.getOrElse(Key("database.password", stringType)) { "" }
  return HikariDataSource(hikariConfig)
}

fun connect(config: Configuration): Database =
  connect(dataSource(config))

fun config(): Configuration = EnvironmentVariables() overriding systemProperties()
