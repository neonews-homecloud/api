package de.neonew.homecloud.logging

import io.github.oshai.kotlinlogging.KLogging
import io.ktor.server.application.ApplicationCallPipeline
import io.ktor.server.application.ApplicationCallPipeline.ApplicationPhase.Monitoring
import io.ktor.server.application.BaseApplicationPlugin
import io.ktor.server.application.call
import io.ktor.server.logging.toLogString
import io.ktor.util.AttributeKey
import kotlin.system.measureTimeMillis

object TimedCallLogging : BaseApplicationPlugin<ApplicationCallPipeline, Any, TimedCallLogging>, KLogging() {
  override val key: AttributeKey<TimedCallLogging> = AttributeKey("Timed Call Logging")

  override fun install(pipeline: ApplicationCallPipeline, configure: Any.() -> Unit): TimedCallLogging {
    pipeline.intercept(Monitoring) {
      val millis = measureTimeMillis { proceed() }
      logger.info { "${call.response.status() ?: "Unhandled"} ${call.request.toLogString()} in $millis ms" }
    }

    return this
  }
}
