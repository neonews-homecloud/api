package de.neonew.homecloud

import de.neonew.homecloud.api.authenticate
import de.neonew.homecloud.api.authenticated
import de.neonew.homecloud.api.signUp
import de.neonew.homecloud.thumbnails.thumbnail
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Copy
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Mkcol
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Move
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Propfind
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Proppatch
import de.neonew.homecloud.webdav.WebDAVResource
import de.neonew.homecloud.webdav.browseDirectory
import de.neonew.homecloud.webdav.copy
import de.neonew.homecloud.webdav.createDirectory
import de.neonew.homecloud.webdav.delete
import de.neonew.homecloud.webdav.downloadFile
import de.neonew.homecloud.webdav.mkcol
import de.neonew.homecloud.webdav.move
import de.neonew.homecloud.webdav.propfind
import de.neonew.homecloud.webdav.proppatch
import de.neonew.homecloud.webdav.uploadFile
import io.ktor.http.HttpHeaders.Allow
import io.ktor.http.HttpHeaders.DAV
import io.ktor.http.HttpMethod.Companion.Delete
import io.ktor.http.HttpMethod.Companion.Get
import io.ktor.http.HttpMethod.Companion.Head
import io.ktor.http.HttpMethod.Companion.Options
import io.ktor.http.HttpMethod.Companion.Put
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.Parameters
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.ApplicationCallPipeline.ApplicationPhase.Call
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.authentication
import io.ktor.server.response.header
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.Routing
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.options
import io.ktor.server.routing.post
import io.ktor.server.routing.put
import io.ktor.server.routing.route
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database
import webFrontend
import java.io.File
import java.io.File.separator
import java.time.Clock

fun Routing.routes(
  database: Database,
  mutex: Mutex,
  filesystem: Filesystem,
  webFrontend: File,
  clock: Clock,
  secret: String,
) {
  get("/status") { call.respond(OK) }
  get("/version") { version() }
  route("/files/{name...}") {
    intercept(Call) { call.response.header(DAV, "1") }
    options {
      val methods = listOf(Options, Head, Get, Put, Delete, Propfind, Proppatch, Mkcol, Copy, Move)
      call.response.header(Allow, methods.joinToString(", ") { it.value })
      call.respond(OK)
    }
    auth {
      get { downloadFile(call.getResource(), getUserFilesystem(filesystem)) }
      put { uploadFile(call.getResource(), getUserFilesystem(filesystem), database, mutex, clock) }
      delete { delete(call.getResource(), getUserFilesystem(filesystem)) }
      propfind { browseDirectory(call.getResource(), getUserFilesystem(filesystem)) }
      proppatch { proppatch() }
      mkcol { createDirectory(call.getResource(), getUserFilesystem(filesystem)) }
      copy { copy(call.getResource(), getUserFilesystem(filesystem), database, mutex) }
      move { move(call.getResource(), getUserFilesystem(filesystem)) }
    }
  }
  auth {
    get("/thumbnails/{name...}") { thumbnail(call.getResource(), getUserFilesystem(filesystem)) }
  }
  route("/api") {
    post("/users") { signUp(filesystem, database, clock, mutex, secret) }
    post("/authenticate") { authenticate(database, clock, secret) }
    auth {
      get("/authenticated") { authenticated() }
    }
  }
  webFrontend(webFrontend, database)
}

fun Parameters.getFullTailcard(name: String): String? =
  getAll(name)?.joinToString(separator)

private fun PipelineContext<*, ApplicationCall>.getUserFilesystem(filesystem: Filesystem): UserFilesystem =
  UserFilesystem(filesystem, call.authentication.principal()!!)

private fun Route.auth(build: Route.() -> Unit) = authenticate("basic", "jwt", build = build)

private fun ApplicationCall.getResource(): WebDAVResource =
  WebDAVResource(parameters.getFullTailcard("name")!!)
