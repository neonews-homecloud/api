package de.neonew.homecloud.api

import de.neonew.homecloud.User
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind.LONG
import kotlinx.serialization.descriptors.PrimitiveKind.STRING
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object UserSerializer : KSerializer<User> {
  override val descriptor = buildClassSerialDescriptor("User") {
    element("id", PrimitiveSerialDescriptor("id", LONG))
    element("name", PrimitiveSerialDescriptor("name", STRING))
  }

  override fun serialize(encoder: Encoder, value: User) {
    val structure = encoder.beginStructure(descriptor)
    structure.encodeLongElement(descriptor, 0, value.id.value)
    structure.encodeStringElement(descriptor, 1, value.name)
    structure.endStructure(descriptor)
  }

  override fun deserialize(decoder: Decoder): User {
    throw NotImplementedError("Deserialize for this class is not supported")
  }
}
