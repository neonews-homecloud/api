package de.neonew.homecloud.authentication

import de.neonew.homecloud.User
import de.neonew.homecloud.Users
import de.neonew.homecloud.authentication.PasswordHasher.verify
import io.ktor.server.auth.AuthenticationConfig
import io.ktor.server.auth.UserPasswordCredential
import io.ktor.server.auth.basic
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
fun AuthenticationConfig.basic(database: Database): Unit =
  basic("basic") {
    validate { verify(it, database) }
  }

fun verify(credential: UserPasswordCredential, database: Database): User? =
  transaction(database) {
    User.find { Users.name eq credential.name }
      .firstOrNull { verify(it.hashedPassword, credential.password) }
  }
