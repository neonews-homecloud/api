package de.neonew.homecloud.authentication

import com.auth0.jwt.JWT.create
import com.auth0.jwt.JWT.require
import com.auth0.jwt.JWTVerifier.BaseVerification
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.algorithms.Algorithm.HMAC256
import de.neonew.homecloud.User
import io.ktor.http.auth.HttpAuthHeader
import io.ktor.http.auth.HttpAuthHeader.Single
import io.ktor.server.auth.AuthenticationConfig
import io.ktor.server.auth.basicAuthenticationCredentials
import io.ktor.server.auth.jwt.jwt
import io.ktor.server.auth.parseAuthorizationHeader
import io.ktor.server.request.ApplicationRequest
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Clock
import java.time.Instant
import java.time.Instant.now
import java.util.Date.from

fun AuthenticationConfig.jwt(database: Database, clock: Clock, secret: String): Unit =
  jwt("jwt") {
    verifier((require(secret.toJWTAlgorithm()) as BaseVerification).build(clock))
    validate {
      transaction(database) {
        User.findById(it.payload.subject.toLong())
      }
    }
    authHeader {
      it.request.getJwtFromBasicAuth()
        ?: it.request.getJwtFromCookie()
        ?: it.request.parseAuthorizationHeader()
    }
  }

fun createJWT(user: User, expiresAt: Instant, clock: Clock, secret: String): String = create()
  .withSubject(user.id.value.toString())
  .withIssuedAt(from(now(clock)))
  .withExpiresAt(from(expiresAt))
  .sign(secret.toJWTAlgorithm())

private fun String.toJWTAlgorithm(): Algorithm = HMAC256(this)

private fun ApplicationRequest.getJwtFromBasicAuth(): HttpAuthHeader? = this.basicAuthenticationCredentials()
  .takeIf { it?.name.equals("token", ignoreCase = true) }
  ?.password
  ?.toBearerAuthHeader()

private fun ApplicationRequest.getJwtFromCookie(): HttpAuthHeader? = call.request.cookies["token"]
  ?.toBearerAuthHeader()

private fun String?.toBearerAuthHeader(): HttpAuthHeader? = this?.let { Single("Bearer", it) }
