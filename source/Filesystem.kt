package de.neonew.homecloud

import de.neonew.homecloud.thumbnails.ThumbnailQueueItem
import de.neonew.homecloud.webdav.WebDAVResource
import io.ktor.http.ContentType
import io.ktor.http.ContentType.Companion.Any
import io.ktor.http.ContentType.Companion.parse
import io.ktor.http.ContentType.Image
import io.ktor.http.encodeURLPath
import io.ktor.util.combineSafe
import kotlinx.coroutines.sync.Mutex
import org.jetbrains.exposed.sql.Database
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Files.probeContentType
import java.nio.file.StandardCopyOption.REPLACE_EXISTING
import java.time.Instant

class Filesystem(val directory: File) {
  fun createUserDirectory(user: User): Boolean =
    userDirectory(user).mkdir() && thumbnailDirectory(user).mkdir()

  internal fun userDirectory(user: User): File =
    directory.combineSafe("users").combineSafe(user.id.value.toString())

  internal fun thumbnailDirectory(user: User): File =
    directory.combineSafe("thumbnails").combineSafe(user.id.value.toString())
}

class UserFilesystem(filesystem: Filesystem, private val user: User) {
  private val userDirectory = filesystem.userDirectory(user)
  private val thumbnailDirectory = filesystem.thumbnailDirectory(user)

  fun read(resource: WebDAVResource): File? {
    val file = file(resource)
    return when (file.exists() && file.isFile) {
      true -> file
      false -> null
    }
  }

  fun isDirectory(resource: WebDAVResource): Boolean {
    val file = file(resource)
    return file.exists() && file.isDirectory
  }

  suspend fun store(resource: WebDAVResource, stream: InputStream,
                    lastModified: Instant,
                    database: Database,
                    mutex: Mutex) {
    stream.toFile(file(resource), lastModified)
    if (contentType(resource).match(Image.Any)) {
      writeTransaction(database, mutex) {
        ThumbnailQueueItem.new {
          user = this@UserFilesystem.user
          this.resource = resource.toString()
        }
      }
    }
  }

  suspend fun copy(source: WebDAVResource,
                   destination: WebDAVResource,
                   database: Database,
                   mutex: Mutex) {
    copy(file(source), file(destination), database, mutex)
  }

  suspend fun copyRecursively(source: WebDAVResource,
                              destination: WebDAVResource,
                              database: Database,
                              mutex: Mutex) =
    file(source).walk().toList()
      .associateWith { file(destination).resolve(it.relativeTo(file(source))) }
      .forEach { (source, destination) ->
        when {
          source.isDirectory -> {
            if (!destination.exists() && !destination.mkdirs())
              throw FileSystemException(file = source, other = destination, reason = "Failed to create destination directory.")
          }
          source.isFile -> copy(source, destination, database, mutex)
        }
      }

  private suspend fun copy(source: File,
                           destination: File,
                           database: Database,
                           mutex: Mutex) {
    source.copyTo(destination, true)
    destination.setLastModified(source.lastModified())
    if (contentType(destination).match(Image.Any)) {
      writeTransaction(database, mutex) {
        ThumbnailQueueItem.new {
          user = this@UserFilesystem.user
          resource = resource(destination).toString()
        }
      }
    }
  }

  fun move(source: WebDAVResource, destination: WebDAVResource) {
    Files.move(file(source).toPath(), file(destination).toPath(), REPLACE_EXISTING)

    if (thumbnailDirectory(source).exists()) {
      Files.move(thumbnailDirectory(source).toPath(), thumbnailDirectory(destination).toPath(), REPLACE_EXISTING)
    }
  }

  fun browseDirectory(resource: WebDAVResource, depth: Int): Sequence<WebDAVResource> =
    when (depth) {
      0 -> sequenceOf(resource)
      else -> file(resource)
        .walk()
        .maxDepth(depth)
        .sorted()
        .map { resource(it) }
    }

  fun createDirectory(resource: WebDAVResource): Boolean =
    file(resource).mkdir()

  fun delete(resource: WebDAVResource) {
    file(resource).deleteRecursively()

    if (thumbnailDirectory(resource).exists()) {
      thumbnailDirectory(resource).deleteRecursively()
    }
  }

  fun thumbnail(resource: WebDAVResource): File =
    thumbnailDirectory(resource).combineSafe("256.jpg")

  fun exists(resource: WebDAVResource): Boolean =
    file(resource).exists()

  fun parentExists(resource: WebDAVResource): Boolean =
    file(resource).parentFile.exists()

  fun contentType(resource: WebDAVResource): ContentType = contentType(file(resource))

  fun location(resource: WebDAVResource) =
    "/files/$resource".encodeURLPath()

  fun file(resource: WebDAVResource): File = userDirectory.combineSafe(resource.resource)

  internal fun contentType(file: File): ContentType =
    probeContentType(file.toPath())?.let { parse(it) } ?: Any

  internal fun thumbnailDirectory(resource: WebDAVResource): File = thumbnailDirectory.combineSafe(resource.resource)

  private fun resource(file: File): WebDAVResource = WebDAVResource(file.relativeTo(userDirectory).path)
}

private fun InputStream.toFile(file: File, lastModified: Instant): File {
  file.outputStream().use { this.copyTo(it) }
  file.setLastModified(lastModified.toEpochMilli())
  return file
}
