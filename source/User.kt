package de.neonew.homecloud

import de.neonew.homecloud.api.UserSerializer
import io.ktor.server.auth.Principal
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable

object Users : LongIdTable() {
  val name = varchar("name", length = 50)
  val hashedPassword = varchar("hashed_password", length = 200)

  init {
    index(true, name)
  }
}

@Serializable(with = UserSerializer::class)
class User(id: EntityID<Long>) : LongEntity(id), Principal {
  companion object : LongEntityClass<User>(Users)

  var name by Users.name
  var hashedPassword by Users.hashedPassword
}
