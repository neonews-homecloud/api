import de.neonew.homecloud.User
import de.neonew.homecloud.getFullTailcard
import io.ktor.server.application.call
import io.ktor.server.http.content.*
import io.ktor.server.response.respond
import io.ktor.server.response.respondRedirect
import io.ktor.server.routing.*
import io.ktor.util.combineSafe
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

fun Routing.webFrontend(webFrontend: File, database: Database) {
  files(File("."), File("index.html"), webFrontend, database)
}

private fun Route.files(folder: File, default: File, webFrontend: File, database: Database) =
  get("{path...}") {
    val relativePath = call.parameters.getFullTailcard("path") ?: return@get

    relativePath.takeIf { it.isEmpty() && showInstallationWizard(database) }?.let {
      call.respondRedirect("/installation")
    } ?: run {
      val dir = webFrontend.combine(folder)
      val file = dir.combineSafe(relativePath).takeIf { it.isFile } ?: dir.combine(default)
      if (file.isFile) {
        call.respond(LocalFileContent(file))
      }
    }
  }

private fun showInstallationWizard(database: Database): Boolean = transaction(database) {
  User.count() == 0L
}

private fun File?.combine(file: File) = when {
  this == null -> file
  else -> resolve(file)
}
