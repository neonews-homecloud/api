package de.neonew.homecloud.thumbnails

import de.neonew.homecloud.User
import de.neonew.homecloud.Users
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable

object ThumbnailQueue : LongIdTable() {
  val user = reference("user_id", Users)
  val resource = varchar("resource", length = 255)
  val lockedBy = varchar("locked_by", length = 255).nullable()

  init {
    index(true, user, resource)
  }
}

class ThumbnailQueueItem(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<ThumbnailQueueItem>(ThumbnailQueue)

  var user by User referencedOn ThumbnailQueue.user
  var resource by ThumbnailQueue.resource
  var lockedBy by ThumbnailQueue.lockedBy
}
