package de.neonew.homecloud.thumbnails

import de.neonew.homecloud.UserFilesystem
import de.neonew.homecloud.webdav.WebDAVResource
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.http.content.LocalFileContent
import io.ktor.server.response.respond
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.thumbnail(resource: WebDAVResource, filesystem: UserFilesystem) {
  if (filesystem.read(resource) == null) {
    return call.respond(NotFound)
  }

  val thumbnail = filesystem.thumbnail(resource).takeIf { it.exists() && it.isFile } ?: return call.respond(NotFound)
  call.respond(LocalFileContent(thumbnail, filesystem.contentType(thumbnail)))
}
