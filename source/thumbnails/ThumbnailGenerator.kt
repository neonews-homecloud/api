package de.neonew.homecloud.thumbnails

import de.neonew.homecloud.Filesystem
import de.neonew.homecloud.UserFilesystem
import de.neonew.homecloud.webdav.WebDAVResource
import de.neonew.homecloud.writeTransaction
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.sync.Mutex
import net.coobird.thumbnailator.Thumbnails
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.io.File
import java.lang.Thread.currentThread
import kotlin.system.measureTimeMillis

private val logger = KotlinLogging.logger {}

fun launchThumbnailGenerationConsumer(database: Database, mutex: Mutex, filesystem: Filesystem): List<Job> {
  resetAllLocks(database, mutex)
  return (1..4).map { launchProcessor(database, mutex, filesystem) }
}

private fun resetAllLocks(database: Database, mutex: Mutex) = runBlocking {
  writeTransaction(database, mutex) {
    ThumbnailQueue.update { it[lockedBy] = null }
  }
}

private fun launchProcessor(database: Database, mutex: Mutex, filesystem: Filesystem) = CoroutineScope(IO).launch {
  while (isActive) {
    handleNextQueueItem(database, mutex) { item ->
      val user = transaction(database) { item.user }
      val resource = WebDAVResource(item.resource)

      val userFilesystem = UserFilesystem(filesystem, user)
      val file = userFilesystem.file(resource)

      if (file.exists() && file.isFile) {
        val directory = userFilesystem.thumbnailDirectory(resource)
        if (!directory.exists() && !userFilesystem.thumbnailDirectory(resource).mkdirs()) {
          throw IllegalStateException("Couldn't create thumbnail directory for resource: ${resource.resource}")
        }
        val millis = measureTimeMillis { generateThumbnail(file, userFilesystem.thumbnail(resource)) }
        logger.debug { "Generated thumbnail for ${user.id} $resource in $millis ms" }
      }
    }
  }
}

private suspend fun handleNextQueueItem(database: Database, mutex: Mutex, block: (ThumbnailQueueItem) -> Unit) {
  val item = popNextQueueItem(currentThread().name, database, mutex)
  block(item)
  writeTransaction(database, mutex) { item.delete() }
}

private suspend fun popNextQueueItem(lockedBy: String, database: Database, mutex: Mutex): ThumbnailQueueItem {
  var item: ThumbnailQueueItem? = null
  var updated = 0
  do {
    writeTransaction(database, mutex) {
      item = ThumbnailQueueItem.find { ThumbnailQueue.lockedBy.isNull() }.firstOrNull()
      updated = item?.let {
        ThumbnailQueue.update({ ThumbnailQueue.id eq it.id and ThumbnailQueue.lockedBy.isNull() }) {
          it[ThumbnailQueue.lockedBy] = lockedBy
        }
      } ?: 0
    }
    delay(500)
  } while (item == null || updated == 0)

  return item!!
}

private fun generateThumbnail(source: File, target: File) {
  Thumbnails.of(source)
    .size(256, 256)
    .outputFormat("jpg")
    .toFile(target)

  target.setLastModified(source.lastModified())
}
