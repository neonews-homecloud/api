package de.neonew.homecloud.migrations

import com.natpryce.konfig.Key
import com.natpryce.konfig.stringType
import de.neonew.homecloud.config
import de.neonew.homecloud.dataSource
import io.ktor.util.combineSafe
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Scope
import liquibase.changelog.ChangeLogParameters
import liquibase.changelog.visitor.ChangeExecListener
import liquibase.command.CommandScope
import liquibase.command.core.UpdateCommandStep
import liquibase.command.core.UpdateCommandStep.COMMAND_NAME
import liquibase.command.core.helpers.ChangeExecListenerCommandStep
import liquibase.command.core.helpers.DatabaseChangelogCommandStep
import liquibase.command.core.helpers.DbUrlConnectionCommandStep
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import java.io.File
import java.io.OutputStream
import javax.sql.DataSource

fun main() {
  val config = config()
  val directory = File(config[Key("directory", stringType)])

  runBlocking { runMigrations(dataSource(config), directory) }
}

val databaseMigrationMutex = Mutex()

suspend fun runMigrations(dataSource: DataSource, directory: File) =
  // Liquibase does not work in parallel execution, it is not thread-safe
  databaseMigrationMutex.withLock {
    dataSource.connection.use { connection ->
      val database = DatabaseFactory.getInstance()
        .findCorrectDatabaseImplementation(JdbcConnection(connection))

      val changelog = "de/neonew/homecloud/database/changelog.yaml"

      val scopeObjects = mapOf(
        Scope.Attr.database.name to database,
        Scope.Attr.resourceAccessor.name to ClassLoaderResourceAccessor()
      )

      Scope.child(scopeObjects) {
        val updateCommand = CommandScope(*COMMAND_NAME).apply {
          addArgumentValue(DbUrlConnectionCommandStep.DATABASE_ARG, database)
          addArgumentValue(UpdateCommandStep.CHANGELOG_FILE_ARG, changelog)
          addArgumentValue(UpdateCommandStep.CONTEXTS_ARG, Contexts().toString())
          addArgumentValue(UpdateCommandStep.LABEL_FILTER_ARG, LabelExpression().originalString)
          addArgumentValue(ChangeExecListenerCommandStep.CHANGE_EXEC_LISTENER_ARG, null as ChangeExecListener?)
          addArgumentValue(DatabaseChangelogCommandStep.CHANGELOG_PARAMETERS, ChangeLogParameters(database))
          setOutput(OutputStream.nullOutputStream()) // Suppress output to stdout (logging will still occur)
        }
        updateCommand.execute()
      }
    }

    directory.combineSafe("users").mkdir()
    directory.combineSafe("thumbnails").mkdir()
  }
