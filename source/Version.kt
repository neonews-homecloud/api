package de.neonew.homecloud

import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.util.pipeline.PipelineContext
import kotlinx.serialization.Serializable
import java.util.Properties

@Serializable
private data class Version(val commitId: String, val time: String, val tag: String)

suspend fun PipelineContext<Unit, ApplicationCall>.version() {
  val properties = properties("/git.properties")
  call.respond(
    Version(
      commitId = properties.getProperty("git.commit.id.abbrev", "n/a"),
      time = properties.getProperty("git.commit.time", "n/a"),
      tag = properties.getProperty("git.tags", "n/a")
    )
  )
}

private fun properties(filename: String): Properties =
  resource(filename).use {
    Properties().apply { load(it) }
  }
