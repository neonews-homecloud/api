package de.neonew.homecloud.tests.integration.matchers

import io.kotest.assertions.Actual
import io.kotest.assertions.Expected
import io.kotest.assertions.intellijFormatError
import io.kotest.assertions.print.printed
import io.kotest.matchers.MatcherResult
import io.kotest.matchers.neverNullMatcher

fun beEqualIgnoringWhitespaces(other: String) = neverNullMatcher<String> { value ->
  MatcherResult(
    value.removeWhitespaces() == other.removeWhitespaces(),
    { intellijFormatError(Expected(other.printed()), Actual(value.printed())) },
    { "${value.printed().value} should not be equal ignoring whitespaces ${other.printed().value}" }
  )
}

private fun String.removeWhitespaces(): String = Regex("\\s").replace(this, "")
