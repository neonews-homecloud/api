package de.neonew.homecloud.tests.integration

import io.kotest.core.config.AbstractProjectConfig

@Suppress("unused")
object ProjectConfig : AbstractProjectConfig() {
  override val parallelism = 8
}
