package de.neonew.homecloud.tests.integration

import io.kotest.assertions.timing.eventually
import kotlin.time.Duration.Companion.seconds

suspend fun <T> eventually(body: suspend () -> T): T = eventually(10.seconds, body)
