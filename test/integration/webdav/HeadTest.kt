package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.matchers.shouldBe
import io.ktor.client.request.head
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType.Text.Plain
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.NotModified
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.contentLength
import io.ktor.http.contentType
import io.ktor.http.ifModifiedSince
import io.ktor.http.withCharset
import java.time.Instant.now
import java.time.Instant.parse
import java.util.Date
import kotlin.text.Charsets.UTF_8

class HeadTest : IntegrationTest({
  "HEAD to /files should work" {
    client.uploadFile("/example.txt")

    val response: HttpResponse = client.head("/files/example.txt")

    response.status shouldBe OK
    response.headers["DAV"] shouldBe "1"
    response.contentType() shouldBe Plain
    response.contentLength() shouldBe 75
    response.lastModified()?.toInstant() shouldBe parse("2020-01-30T09:59:59Z")
    response.bodyAsText() shouldBe ""
  }

  "HEAD to non-existent file in /files should return HTTP 404" {
    val response: HttpResponse = client.head("/files/example.txt")
    response.status shouldBe NotFound
  }

  "HEAD to directory should return HTTP 200" {
    val response: HttpResponse = client.head("/files/")
    response.status shouldBe OK
    response.contentType() shouldBe Plain.withCharset(UTF_8)
    response.contentLength() shouldBe 68
    response.bodyAsText() shouldBe ""
  }

  "HEAD with using the cache (If-Modified-Since)" {
    client.uploadFile("/example.txt")

    clock.advanceByHours(2)
    val response: HttpResponse = client.head("/files/example.txt") {
      ifModifiedSince(Date.from(now(clock)))
    }

    response.status shouldBe NotModified
    response.headers["DAV"] shouldBe "1"
    response.contentType() shouldBe null
    response.lastModified()?.toInstant() shouldBe parse("2020-01-30T09:59:59Z")
    // This should be "null", see also here: https://github.com/heroku/cowboyku/issues/9
    // Possible bug in ConditionalHeaders?
    response.contentLength() shouldBe 0
  }
})
