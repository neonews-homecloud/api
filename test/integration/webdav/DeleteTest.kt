package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.matchers.file.exist
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.request.delete
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound

class DeleteTest : IntegrationTest({
  "DELETE a file should work" {
    client.uploadFile("/example.txt")

    val response: HttpResponse = client.delete("/files/example.txt")

    response.status shouldBe NoContent
    response.headers["DAV"] shouldBe "1"
    userFile("example.txt") shouldNot exist()
  }

  "DELETE an empty directory should work" {
    client.mkcol("/files/example")

    val response: HttpResponse = client.delete("/files/example")

    response.status shouldBe NoContent
    userFile("example") shouldNot exist()
  }

  "DELETE a non-empty directory should work" {
    client.mkcol("/files/example")
    client.uploadFile("/example.txt", "/example/example.txt")

    val response: HttpResponse = client.delete("/files/example")

    response.status shouldBe NoContent
    userFile("example") shouldNot exist()
  }

  "DELETE a non-existent file should not work" {
    val response: HttpResponse = client.delete("/files/example.txt")
    response.status shouldBe NotFound
  }
})
