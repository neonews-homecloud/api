package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Mkcol
import io.kotest.matchers.shouldBe
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.Conflict
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.MethodNotAllowed
import io.ktor.http.HttpStatusCode.Companion.UnsupportedMediaType
import io.ktor.http.contentType

class CreateDirectoryTest : IntegrationTest({
  "MKCOL to /files should create a new directory" {
    val response: HttpResponse = client.mkcol("/files/new%20directory")

    response.status shouldBe Created
    response.headers["DAV"] shouldBe "1"
    response.headers["Location"] shouldBe "/files/new%20directory"
  }

  "MKCOL to existing directory should fail" {
    client.mkcol("/files/new-directory")
    val response: HttpResponse = client.mkcol("/files/new-directory")

    response.status shouldBe MethodNotAllowed
  }

  "MKCOL with body should fail" {
    val response: HttpResponse = client.mkcol("/files/new-directory") {
      setBody("something")
      contentType(Json)
    }

    response.status shouldBe UnsupportedMediaType
  }

  "MKCOL shouldn't create intermediate collection" {
    val response: HttpResponse = client.mkcol("/files/non-existent-subdir/new-directory")

    response.status shouldBe Conflict
  }
})

internal suspend fun HttpClient.mkcol(url: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  request {
    method = Mkcol
    url(url)
    apply(block)
  }
