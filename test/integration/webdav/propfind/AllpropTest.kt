package de.neonew.homecloud.tests.integration.webdav.propfind

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beEqualIgnoringWhitespaces
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType.Application.Xml
import io.ktor.http.HttpStatusCode.Companion.MultiStatus
import io.ktor.http.contentType

class AllpropTest : IntegrationTest({
  "PROPFIND with allprop" {
    val response: HttpResponse = client.propfind("/files/") {
      setBody(
        """<?xml version="1.0" encoding="utf-8" ?>
            <D:propfind xmlns:D="DAV:">
              <D:allprop/>
            </D:propfind>
        """.trimIndent()
      )
      contentType(Xml)
    }

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces(
      """<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
  <d:response>
    <d:href>/files/</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified>Thu, 30 Jan 2020 09:59:59 GMT</d:getlastmodified>
        <d:resourcetype>
          <d:collection/>
        </d:resourcetype>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
    <d:propstat>
      <d:prop>
				<d:getcontentlength/>
				<d:getcontenttype/>
        <d:getetag/>
        <d:quota-used-bytes/>
        <d:quota-available-bytes/>
      </d:prop>
      <d:status>HTTP/1.1 404 Not Found</d:status>
    </d:propstat>
  </d:response>
</d:multistatus>"""
    )
  }
})
