package de.neonew.homecloud.tests.integration.webdav.propfind

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beEqualIgnoringWhitespaces
import de.neonew.homecloud.tests.integration.webdav.mkcol
import de.neonew.homecloud.tests.integration.webdav.uploadFile
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType.Application.Xml
import io.ktor.http.HttpStatusCode.Companion.MultiStatus
import io.ktor.http.contentType
import io.ktor.http.withCharset
import kotlin.text.Charsets.UTF_8

class PropnameTest : IntegrationTest({
  "PROPFIND with propname" {
    client.uploadFile("/example.txt", "/example1.txt")
    client.mkcol("/files/example2")

    val response: HttpResponse = client.propfind("/files/") {
      setBody(
        """
          <?xml version="1.0" encoding="utf-8" ?>
          <D:propfind xmlns:D="DAV:">
            <D:propname/>
          </D:propfind>
        """.trimIndent()
      )
      contentType(Xml)
    }

    response.status shouldBe MultiStatus
    response.contentType() shouldBe Xml.withCharset(UTF_8)
    response.headers["DAV"] shouldBe "1"
    response.bodyAsText() should beEqualIgnoringWhitespaces("""<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
  <d:response>
    <d:href>/files/</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified/>
        <d:resourcetype/>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
  </d:response>
  <d:response>
    <d:href>/files/example1.txt</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified/>
        <d:getcontentlength/>
        <d:resourcetype/>
        <d:getcontenttype/>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
  </d:response>
  <d:response>
    <d:href>/files/example2</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified/>
        <d:resourcetype/>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
  </d:response>
</d:multistatus>""")
  }
})
