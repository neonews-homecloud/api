package de.neonew.homecloud.tests.integration.webdav.propfind

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beEqualIgnoringWhitespaces
import de.neonew.homecloud.tests.integration.webdav.mkcol
import de.neonew.homecloud.tests.integration.webdav.uploadFile
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.ktor.client.request.header
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType.Application.Xml
import io.ktor.http.HttpStatusCode.Companion.MultiStatus
import io.ktor.http.contentType

class BrowseDepthTest : IntegrationTest({
  beforeTest {
    client.mkcol("/files/level1")
    client.mkcol("/files/level1/level2")
    client.mkcol("/files/level1/level2/level3")
    client.uploadFile("/example.txt", "/level1/level2/level3/example.txt")
  }

  "PROPFIND with no Depth header" {
    val response: HttpResponse = propfind(depth = null)

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces(
      """<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2/level3</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2/level3/example.txt</d:href>
	</d:response>
</d:multistatus>"""
    )
  }

  "PROPFIND with Depth: infinity" {
    val response: HttpResponse = propfind(depth = "infinity")

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces(
      """<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2/level3</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1/level2/level3/example.txt</d:href>
	</d:response>
</d:multistatus>"""
    )
  }

  "PROPFIND with Depth: 0" {
    val response: HttpResponse = propfind(depth = "0")

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces(
      """<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/</d:href>
	</d:response>
</d:multistatus>"""
    )
  }

  "PROPFIND with Depth: 1" {
    val response: HttpResponse = propfind(depth = "1")

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces(
      """<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/</d:href>
	</d:response>
	<d:response>
		<d:href>/files/level1</d:href>
	</d:response>
</d:multistatus>"""
    )
  }
})

private suspend fun IntegrationTest.propfind(depth: String?): HttpResponse =
  client.propfind("/files/") {
    setBody(
      """
        <?xml version="1.0" encoding="utf-8" ?>
        <D:propfind xmlns:D="DAV:">
          <D:prop/>
        </D:propfind>
      """.trimIndent()
    )
    header("Depth", depth)
    contentType(Xml)
  }
