package de.neonew.homecloud.tests.integration.webdav.propfind

import de.neonew.homecloud.tests.integration.IntegrationTest
import de.neonew.homecloud.tests.integration.matchers.beEqualIgnoringWhitespaces
import de.neonew.homecloud.tests.integration.webdav.uploadContent
import de.neonew.homecloud.tests.integration.webdav.uploadFile
import de.neonew.homecloud.webdav.WebDAVMethod.Companion.Propfind
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType.Application.Xml
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.MultiStatus
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.contentType
import io.ktor.http.withCharset
import io.ktor.util.combineSafe
import java.time.Instant
import kotlin.text.Charsets.UTF_8

class BrowseTest : IntegrationTest({
  "PROPFIND to /files should return a list of files" {
    client.uploadFile("/example.txt", "/example1.txt")
    client.uploadContent("", "/example2")

    clock.set(Instant.parse("2020-02-28T19:59:59.123Z"))
    directory.combineSafe("users/1").setLastModified(clock.millis())

    val response: HttpResponse = client.propfind("/files/")

    response.status shouldBe MultiStatus
    response.contentType() shouldBe Xml.withCharset(UTF_8)
    response.headers["DAV"] shouldBe "1"
    response.bodyAsText() should beEqualIgnoringWhitespaces("""<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
  <d:response>
    <d:href>/files/</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified>Fri, 28 Feb 2020 19:59:59 GMT</d:getlastmodified>
        <d:resourcetype>
          <d:collection/>
        </d:resourcetype>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
    <d:propstat>
      <d:prop>
				<d:getcontentlength/>
				<d:getcontenttype/>
        <d:getetag/>
        <d:quota-used-bytes/>
        <d:quota-available-bytes/>
      </d:prop>
      <d:status>HTTP/1.1 404 Not Found</d:status>
    </d:propstat>
  </d:response>
  <d:response>
    <d:href>/files/example1.txt</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified>Thu, 30 Jan 2020 09:59:59 GMT</d:getlastmodified>
        <d:getcontentlength>75</d:getcontentlength>
        <d:resourcetype/>
        <d:getcontenttype>text/plain</d:getcontenttype>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
    <d:propstat>
      <d:prop>
        <d:getetag/>
        <d:quota-used-bytes/>
        <d:quota-available-bytes/>
      </d:prop>
      <d:status>HTTP/1.1 404 Not Found</d:status>
    </d:propstat>
  </d:response>
  <d:response>
    <d:href>/files/example2</d:href>
    <d:propstat>
      <d:prop>
        <d:getlastmodified>Thu, 30 Jan 2020 09:59:59 GMT</d:getlastmodified>
        <d:getcontentlength>0</d:getcontentlength>
        <d:resourcetype/>
        <d:getcontenttype>*/*</d:getcontenttype>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
    <d:propstat>
      <d:prop>
        <d:getetag/>
        <d:quota-used-bytes/>
        <d:quota-available-bytes/>
      </d:prop>
      <d:status>HTTP/1.1 404 Not Found</d:status>
    </d:propstat>
  </d:response>
</d:multistatus>""")
  }

  "PROPFIND with specified props to /files should return a list of files" {
    client.uploadFile("/example.txt")

    val response: HttpResponse = client.propfind("/files/") {
      setBody(
        """
          <?xml version="1.0" encoding="utf-8" ?>
          <D:propfind xmlns:D="DAV:">
            <D:prop>
              <D:resourcetype/>
              <D:getcontentlength/>
            </D:prop>
          </D:propfind>
          """.trimIndent()
      )
      contentType(Xml)
    }

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces("""<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
  <d:response>
    <d:href>/files/</d:href>
    <d:propstat>
      <d:prop>
        <d:resourcetype>
          <d:collection/>
        </d:resourcetype>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
    <d:propstat>
      <d:prop>
				<d:getcontentlength/>
      </d:prop>
      <d:status>HTTP/1.1 404 Not Found</d:status>
    </d:propstat>
  </d:response>
  <d:response>
    <d:href>/files/example.txt</d:href>
    <d:propstat>
      <d:prop>
        <d:getcontentlength>75</d:getcontentlength>
        <d:resourcetype/>
      </d:prop>
      <d:status>HTTP/1.1 200 OK</d:status>
    </d:propstat>
  </d:response>
</d:multistatus>""")
  }

  "PROPFIND with a file containing a space" {
    client.uploadFile("/example.txt", "/file%20with%20space.txt")

    val response: HttpResponse = client.propfind("/files/") {
      setBody(
        """
          <?xml version="1.0" encoding="utf-8" ?>
          <D:propfind xmlns:D="DAV:">
            <D:prop/>
          </D:propfind>
        """.trimIndent()
      )
      contentType(Xml)
    }

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces("""<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/</d:href>
	</d:response>
	<d:response>
		<d:href>/files/file%20with%20space.txt</d:href>
	</d:response>
</d:multistatus>""")
  }

  "PROPFIND on a file, not a directory" {
    client.uploadFile("/example.txt")

    val response: HttpResponse = client.propfind("/files/example.txt") {
      setBody(
        """
          <?xml version="1.0" encoding="utf-8" ?>
          <D:propfind xmlns:D="DAV:">
            <D:prop/>
          </D:propfind>
        """.trimIndent()
      )
      contentType(Xml)
    }

    response.status shouldBe MultiStatus
    response.bodyAsText() should beEqualIgnoringWhitespaces("""<?xml version="1.0" encoding="UTF-8"?>
<d:multistatus xmlns:d="DAV:">
	<d:response>
		<d:href>/files/example.txt</d:href>
	</d:response>
</d:multistatus>""")
  }

  "PROPFIND on a non-existent resource" {
    val response: HttpResponse = client.propfind("/files/example.txt")
    response.status shouldBe NotFound
  }

  "PROPFIND with invalid request" {
    val response: HttpResponse = client.propfind("/files/") {
      setBody("<foo>")
      contentType(Xml)
    }

    response.status shouldBe BadRequest
  }
})

internal suspend fun HttpClient.propfind(url: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse =
  request {
    method = Propfind
    url(url)
    apply(block)
  }
