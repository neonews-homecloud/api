package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.matchers.shouldBe
import io.ktor.client.request.options
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.OK

class OptionsTest : IntegrationTest({
  "OPTIONS to /files should return a list of allowed operations" {
    val response: HttpResponse = client.options("/files/")

    response.status shouldBe OK
    response.headers["Allow"] shouldBe "OPTIONS, HEAD, GET, PUT, DELETE, PROPFIND, PROPPATCH, MKCOL, COPY, MOVE"
    response.headers["DAV"] shouldBe "1"
  }
})
