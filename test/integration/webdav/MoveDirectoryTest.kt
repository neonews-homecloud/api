package de.neonew.homecloud.tests.integration.webdav

import de.neonew.homecloud.tests.integration.IntegrationTest
import io.kotest.matchers.file.exist
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.client.request.header
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.PreconditionFailed

class MoveDirectoryTest : IntegrationTest({
  "MOVE a directory should work" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.move("/files/source/", "/files/destination/")

    response.status shouldBe Created
    response.headers["DAV"] shouldBe "1"
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("destination/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
    userFile("source") shouldNot exist()
  }

  "MOVE a directory into sub-directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    val response = client.move("/files/source/", "/files/source/sub-dir")

    response.status shouldBe Forbidden
  }

  "MOVE a directory to existing directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    client.mkcol("/files/destination/")
    client.uploadFile("/example.txt", "/destination/existing.txt")

    val response = client.move("/files/source/", "/files/destination/")

    response.status shouldBe NoContent
    response.headers["Location"] shouldBe "/files/destination/"
    userFile("source") shouldNot exist()
    userFile("destination/example.txt").readText() shouldBe "This is just an example file, which will be stored, viewed and downloaded.\n"
    userFile("destination/existing.txt") shouldNot exist()
  }

  "MOVE don't overwrite directory" {
    client.mkcol("/files/source/")
    client.uploadFile("/example.txt", "/source/example.txt")

    client.mkcol("/files/destination/")
    client.uploadFile("/example.txt", "/destination/existing.txt")

    val response = client.move("/files/source/", "/files/destination/") {
      header("Overwrite", "F")
    }

    response.status shouldBe PreconditionFailed
  }

  "MOVE non-existent directory" {
    val response = client.move("/files/source/", "/files/destination/")

    response.status shouldBe NotFound
  }
})
