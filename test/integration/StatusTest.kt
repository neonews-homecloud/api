package de.neonew.homecloud.tests.integration

import io.kotest.matchers.shouldBe
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode.Companion.OK

class StatusTest : IntegrationTest({
  "/status" {
    val response: HttpResponse = client.get("/status")
    response.status shouldBe OK
  }
})
