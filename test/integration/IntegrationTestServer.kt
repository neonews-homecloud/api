package de.neonew.homecloud.tests.integration

import com.natpryce.konfig.Configuration
import com.natpryce.konfig.ConfigurationProperties
import com.statemachinesystems.mockclock.MockClock
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import de.neonew.homecloud.migrations.runMigrations
import de.neonew.homecloud.server
import io.kotest.common.runBlocking
import io.ktor.client.HttpClient
import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.auth.providers.BasicAuthCredentials
import io.ktor.client.plugins.auth.providers.basic
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.contentType
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.jetty.JettyApplicationEngineBase
import io.ktor.util.combineSafe
import org.eclipse.jetty.server.Server
import java.io.Closeable
import java.io.File
import java.time.Instant.parse
import java.time.ZoneId.of
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class IntegrationTestServer(
  name: String,
  basicAuth: Boolean,
  val directory: File,
  private val jwtSecret: String,
) : Closeable {
  val client: HttpClient
  val clock: MockClock = MockClock.at(parse("2020-01-30T09:59:59.123Z"), of("America/Montreal"))

  private val databaseUrl = "jdbc:h2:mem:$name;DB_CLOSE_DELAY=-1;DATABASE_TO_LOWER=TRUE;SCHEMA=PUBLIC"
  private val databaseDriver = "org.h2.Driver"
  private val server: ApplicationEngine

  private val user = "user"
  private val password = "password"

  init {
    val hikariConfig = HikariConfig()
    hikariConfig.driverClassName = databaseDriver
    hikariConfig.jdbcUrl = databaseUrl
    val dataSource = HikariDataSource(hikariConfig)

    runBlocking { runMigrations(dataSource, directory) }
    directory.setLastModified(clock.millis())

    server = startServer()
    client = createHttpClient(server.port(), basicAuth)
  }

  suspend fun registerUser() {
    val response = client.post("/api/users") {
      setBody("""{"name":"$user","password":"$password"}""")
      contentType(Json)
    }
    if (response.status != Created) {
      throw IllegalStateException("Couldn't register user: " + response.status)
    }
    directory.combineSafe("users/1").setLastModified(clock.millis())
  }

  private fun createHttpClient(port: Int, basicAuth: Boolean): HttpClient =
    HttpClient {
      followRedirects = false
      expectSuccess = false
      if (basicAuth) {
        install(Auth) {
          basic {
            credentials { BasicAuthCredentials(user, this@IntegrationTestServer.password) }
            sendWithoutRequest { true }
          }
        }
      }
      defaultRequest {
        this.host = "localhost"
        this.port = port
      }
    }

  private fun startServer(): ApplicationEngine =
    server(
      mapOf(
        "server.port" to "0",
        "directory" to directory.absolutePath,
        "webFrontend" to "./test/resources/web",
        "database.url" to databaseUrl,
        "database.driver" to databaseDriver,
        "database.username" to "",
        "database.password" to "",
        "jwt.secret" to jwtSecret,
      ).toConfiguration(),
      clock
    ).start()

  override fun close() {
    client.close()
    server.stop(1000, 2000)
  }

  private fun Map<String, String>.toConfiguration(): Configuration = ConfigurationProperties(this.toProperties())
}

fun ApplicationEngine.port(): Int {
  // Ugly reflection access to property: https://github.com/ktorio/ktor/issues/909
  val field = JettyApplicationEngineBase::class.memberProperties.first { it.name == "server" }
  field.isAccessible = true
  val server: Server = field.get(this as JettyApplicationEngineBase) as Server
  return server.uri.port
}
