package de.neonew.homecloud.tests.unit.webdav

import de.neonew.homecloud.webdav.WebDAVResource
import de.neonew.homecloud.webdav.toResource
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class WebDAVResourceTest : StringSpec({
  "WebDAVResource" {
    forAll(
      row("/files/example.txt", WebDAVResource("example.txt")),
      row("files/example.txt", WebDAVResource("example.txt")),
      row("/files/test/files/example.txt", WebDAVResource("test/files/example.txt")),
      row("/files/test/../example.txt", WebDAVResource("example.txt")),
      row("http://url.com/files/example.txt", WebDAVResource("example.txt")),
      row("/test/files/example.txt", null),
      row("/files/../example.txt", null),
      row("/files/", null)
    ) { url, resource ->
      url.toResource() shouldBe resource
    }
  }
})
